
import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='psyblocks',  
     version='0.0.1',
     scripts=['psyblocks'] ,
     author="Eugene Belyaev",
     author_email="bel_eugene@protonmail.com",
     description="A GUI interface to create \
     models and simulate your system.",
     long_description=long_description,
   long_description_content_type="text/markdown",
     url="#",
     packages=setuptools.find_packages(),
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
 )